using UnityEngine;
using System.Collections;

public class SpawnerStarter : MonoBehaviour {
	
	public GameObject spawner;
	private SpawnerScript script;
	
	// Use this for initialization
	void Start () {
		script = spawner.GetComponent<SpawnerScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool startOrStop = true;
	private bool once = true;

	void OnTriggerEnter(Collider other) {
		if(once && other.gameObject.tag.CompareTo("Player") < 1){
	    	//target.gameObject.SetActiveRecursively(true);
			//Debug.Log("STOPPED THE SPAWNING: "+startOrStop + "; player: "+other.tag + "; result: " + other.gameObject.tag.CompareTo("Player"));
			script.startSpawning = startOrStop;
	    	once = false;
	    }
	}
}
