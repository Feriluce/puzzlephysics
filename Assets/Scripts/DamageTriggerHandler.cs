using UnityEngine;
using System.Collections;
//using System;


public class DamageTriggerHandler : MonoBehaviour {
	
	private GameObject player;
	private playerHitScript script;
	public int damageDealt = 10;
	
	void Start () {
	
		player = GameObject.FindGameObjectWithTag("Player"); 
	 	
	 	script = player.GetComponent<playerHitScript>();
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter (Collider col) {
			if(col.gameObject.CompareTag("Player")){
				
				script.dealDamage(damageDealt, col.transform.position, false);
				
			}
	}
	
	void OnTriggerExit (Collider col) {
	/*
			if(col.gameObject.CompareTag("Player")){
				
				for(var i=0; i<objectsToEnable.length; i++){
					objectsToEnable[i].active = false;
					
				}
				
			}
	*/
	}
	
}