using UnityEngine;
using System.Collections;

public class startSpawnerOnHit : MonoBehaviour {
	
	public GameObject spawner;
	private SpawnerScript script;
	
	// Use this for initialization
	void Start () {
		script = spawner.GetComponent<SpawnerScript>();
	}
	
	private bool once = true;
	// Update is called once per frame
	void Update () {
		if(once && transform.position.z > 5){
		    //target.gameObject.SetActiveRecursively(true);
			script.startSpawning = true;
		    once = false;
		}
	}
}
