using UnityEngine;
using System.Collections;

public class teleportObjectScript : MonoBehaviour {
	[HideInInspector]
	public int plane;
	int platDist = 10;					//Current distance between platforms. Change if distance changes.
	
	private enemyScriptNew enemyScript;
	private EnemyScript enemyScriptOld;
	
	private GameObject debugSphere;
	private GameObject debugSphere2;
	private GameObject debugResource;
	private	GameObject shadow;
	private Shader shader4;
	private ApplyCustomShader shadowMesh;
	GameObject player;
	
	// Use this for initialization
	void Start () {
		
		if(transform.position.z < 5 ){
			plane = 1;
		}
		else {
			plane = 0;
		}
		
		//Here I'm accessing a parameter from a JavaScript script from within C#. Yeah!
		//the js script must be placed in the puzzlephysics/Assete/Plugins folder or else it won't be compiled in time for the C# scripts
		enemyScript = GetComponent<enemyScriptNew>();
		if(!enemyScript){
			enemyScriptOld = GetComponent<EnemyScript>();
		}
		
		player = GameObject.FindGameObjectWithTag("Player");
		
		debugResource = (GameObject)Resources.Load("debug Sphere");
		//debugResource.collider.isTrigger = true;
		//debugResource.collider.enabled = false;
		
		shadow = findChildWithTag("enShad");
		
		shader4 = Shader.Find( "AlphaSelfIllum" );
		
		if(shadow && shadow.transform.GetComponentInChildren<ApplyCustomShader>())
			shadowMesh = shadow.transform.GetComponentInChildren<ApplyCustomShader>();
	}
	
	// FIND CHILD WITH TAG
	GameObject findChildWithTag(string tagToFind) {
	    return findChildWithTag(tagToFind, this.gameObject);
	}
	GameObject findChildWithTag(string tagToFind, GameObject startingObject) {
	
	    Component[] childTransforms = startingObject.GetComponentsInChildren<Transform>();
	    foreach (Component thisComponent in childTransforms) { 
	       Transform thisTransform = thisComponent as Transform;
	       if (thisTransform.gameObject.tag == tagToFind) {
	         return thisTransform.gameObject as GameObject;
	       }
	    }
	    return null;
	}
	
	// Update is called once per frame
	void Update () {
		if(shadowMesh)
		if(Mathf.Abs(player.transform.position.z - transform.position.z) < 5){
			//shadowMesh.restoreToCustomShader();
			shadowMesh.transform.renderer.material.color = new Color(shadowMesh.transform.renderer.material.color.r, shadowMesh.transform.renderer.material.color.g, shadowMesh.transform.renderer.material.color.b, 0.2f);
			//shadowMesh.applicableAlpha = 0.2f;
		} else{
			shadowMesh.transform.renderer.material.color = new Color(shadowMesh.transform.renderer.material.color.r, shadowMesh.transform.renderer.material.color.g, shadowMesh.transform.renderer.material.color.b, 0.0f);
			//shadowMesh.applicableAlpha = 0.0f;
		}
	}
	

	void OnCollisionEnter(Collision col){

		foreach(ContactPoint contact in col.contacts){
			if(contact.otherCollider.CompareTag("teleportProjectile") 
				&& contact.thisCollider.CompareTag("teleportObject")){
					if(transform.position.z < 5 ){
						plane = 1;
					}
					else {
						plane = 0;
					}
							
					if(plane == 1){
						bool hit = sphereCheck(10);
						if(!hit){
							if(enemyScript){
								plane = 0;
								enemyScript.currentPlane = 10;
								transform.Translate(Vector3.forward*platDist, Space.World);
							} else if(enemyScriptOld){
								plane = 1;
								enemyScriptOld.currentPlane = 10;
								transform.Translate(Vector3.forward*platDist, Space.World);
							}
							else{
								transform.Translate(Vector3.forward*platDist, Space.World);
								plane = 0;
							}
							
						}
						
					} else {
						bool hit = sphereCheck(-10);
						if(!hit){
							if(enemyScript){
								enemyScript.currentPlane = 0;
								transform.Translate(Vector3.forward*(-platDist), Space.World);
							} else
							if(enemyScriptOld){
								enemyScriptOld.currentPlane = 0;
								transform.Translate(Vector3.forward*(-platDist), Space.World);
							}
							else
								transform.Translate(Vector3.forward*(-platDist), Space.World);
							plane = 1;
						}
						
					}

			}
		}
	}
	
	public float redLifeTime = 1;
	bool sphereCheck(float zWard){
		Rigidbody rigidBody = (Rigidbody)GetComponent(typeof(Rigidbody));
		if(!rigidBody){//then there is a characterController instead
			return capsuleCheck(zWard);
		} 
    
		BoxCollider boxCollider = (BoxCollider)GetComponent(typeof(BoxCollider));
		//float boxCenterY=0;
		//if(boxCollider){
		//	boxCenterY = boxCollider.center.y;
		//}
		float radius = (rigidbody.collider.bounds.size.x > rigidbody.collider.bounds.size.y ? rigidbody.collider.bounds.size.x/2.5f : rigidbody.collider.bounds.size.y/2.5f );
		Vector3 position2 = transform.position + new Vector3(0,0,zWard);
		int mask = 1 << 11;
		mask = ~mask;
		
		if(Physics.CheckSphere(position2, radius, mask)){
			

			if(shadowMesh){
				shadowMesh.transform.renderer.material.shader = shader4;
				shadowMesh.transform.renderer.material.color = new Color(Color.red.r, Color.red.g, Color.red.b, 1);
				
				StartCoroutine("restoreShadowColor");
			} else{
				debugSphere = Instantiate(debugResource, position2, transform.rotation) as GameObject;
				//debugSphere.transform.position = position2;
				debugSphere.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				Destroy (debugSphere, redLifeTime);
			}
			
			//Debug.Log("___________________ [rigidbody] You can't teleport object to: " + position2.z + "; object is on: " + transform.position.z);
			return true;
		}
		else return false;

	}
	
	IEnumerator  restoreShadowColor(){
	  yield return new WaitForSeconds(redLifeTime/1.5f);

	  
	  shadowMesh.transform.renderer.material.color = Color.black;
	  shadowMesh.restoreToCustomShader();
	  //shadowMesh.transform.renderer.material.color.a = 0.2;

      //StartCoroutine("restoreShadowColor");
	}
	/*
	float sweepCastHit(float zWard){
		Rigidbody rigidBody = (Rigidbody)GetComponent(typeof(Rigidbody));
		if(!rigidBody){//then there is a characterController instead
			return capsuleCastHit(zWard);
		}	
		RaycastHit hit = new RaycastHit();
		float distanceToObstacle = -100;
	    
		if(rigidBody.SweepTest(transform.forward, out hit, zWard) ) {
	        distanceToObstacle = hit.distance;
			if(distanceToObstacle < 2)
				Debug.Log("___________________ [rigidbody] THERE HAS BEEN A HIT at: " + hit.distance);
	    }
		
	    
		return distanceToObstacle;
	}
	*/
	
	bool capsuleCheck(float zWard){// capsule without a cast/sweep. just checks the destination: this is what we really need!
			CharacterController charContr = (CharacterController)GetComponent(typeof(CharacterController));
			
			float height = transform.collider.bounds.extents.y;//charContr.height;
		    /*
			Vector3 p1 = transform.position + charContr.center +//transform.collider.bounds.center + 
		                	Vector3.up * (-height/2 + height/9)
							+ new Vector3(0, 0, zWard);
		    Vector3 p2 = p1 + Vector3.up * (height - height/9);// + new Vector3(0, height/2, 0);
		    */
			Vector3 p1;
	    	Vector3 p2;
			float radius;
			if(enemyScript || enemyScriptOld){
				p1 = transform.position + charContr.center +//transform.collider.bounds.center + 
					               	Vector3.up * (-height/8)
									+ new Vector3(0, 0, zWard);
		    	p2 = p1 + Vector3.up * (height/4);// + new Vector3(0, height/2, 0)
				radius = height/2.5f;
			}
			else{
				p1 = transform.position + charContr.center +//transform.collider.bounds.center + 
					               	Vector3.up * (-height/2)
									+ new Vector3(0, 0, zWard);
		    	p2 = p1 + Vector3.up * (height);// + new Vector3(0, height/2, 0)
				radius = charContr.radius;//transform.collider.bounds.extents.x;
			}
			
			
			int mask = 1 << 11;
			mask = ~mask;
	
			if(Physics.CheckCapsule(p1, p2, radius, mask)){
				
			if(shadowMesh){
				shadowMesh.transform.renderer.material.shader = shader4;
				shadowMesh.transform.renderer.material.color = new Color(Color.red.r, Color.red.g, Color.red.b, 1);
				StartCoroutine("restoreShadowColor");
			} else{
				//debugSphere = Instantiate(debugResource, p1, transform.rotation) as GameObject;
				debugSphere = Instantiate(debugResource, p1, transform.rotation) as GameObject;
				//debugSphere.transform.position = p1;
				debugSphere.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				
				debugSphere2 = Instantiate(debugResource, p2, transform.rotation) as GameObject;
				//debugSphere2.transform.position = p2;
				debugSphere2.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				
				Destroy (debugSphere, redLifeTime);
				Destroy (debugSphere2, redLifeTime);
			}
			
			
			
			//Debug.Log("___________________ [character controller] You can't teleport target to: " + p1.z + "; object is on: " + transform.position.z);
			return true;
		}
		else return false;
	}
	/*
	float capsuleCastHit (float zWard) {
	    CharacterController charContr = (CharacterController)GetComponent(typeof(CharacterController));
		//if(!charContr){//then there is a rigidbody instead
		//	return sweepCastHit(zWard);
		//}
		RaycastHit hit = new RaycastHit();
		float distanceToObstacle = -100;
		
		float height = charContr.height;// * 0.98f;//trying to make the capsule slightly smaller than the object. Make sure it doesn't prevent teleportation just because some corner is slightly intersecting an obstacle.
	    Vector3 p1 = transform.position + charContr.center + 
	                	Vector3.up * (-height*0.5f);
	    Vector3 p2 = p1 + Vector3.up * height;
		
	    // Cast character controller shape 10 meters forward, to see if it is about to hit anything
	    if (Physics.CapsuleCast (p1, p2, charContr.radius, transform.forward, out hit, zWard)) {
	        distanceToObstacle = hit.distance;
			if(distanceToObstacle < 2)
				Debug.Log("___________________ [character controller] THERE HAS BEEN A HIT at: " + hit.distance);
	    }
		
		return distanceToObstacle;
	}
	*/
	

	

}

	

/*
//There's a problem with flying enemies teleporting themselves in certain conditions: when a flying enemy is still moving when it fires. It will collide with its own projectile and teleport itself.
				try{
					//don't shoot yourself. TODO: figure out how to get parent of clone...
					//if(gameObject != contact.otherCollider.gameObject >Here< .parent){
					//print ("gameObject: " + gameObject + "; contact.otherCollider.gameObject: " + contact.otherCollider.gameObject); 
				}
				catch(UnityException e){
						//"gameObject" is null...	
				}
*/

