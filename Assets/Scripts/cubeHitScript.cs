using UnityEngine;
using System;

public class cubeHitScript : MonoBehaviour {
	
  public int life = 2;
	
	void Start () {
		
	}
	
	void Update () {
		
	}
	
	void OnCollisionEnter(Collision col){
		foreach(ContactPoint contact in col.contacts){
			if(contact.otherCollider.CompareTag("dmgProjectile")){
				if(--life<=0) {
					Destroy(gameObject);
				}
			}
		}
	}
}