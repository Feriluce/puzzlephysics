using UnityEngine;
using System.Collections;

public class menuScript : MonoBehaviour {
	
	int mScreen;
	
	// Use this for initialization
	void Start () {
		mScreen = 0;
	}
	
	//Just a little testcomment.
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		
		int bWidth = 256;
		int bHeight = 36;
		int bspace = 12;
		
		
		if(mScreen == 0){
			if (!PlayerPrefs.HasKey("level")) GUI.enabled = false;
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2 - (bHeight*2 +bspace/2+bspace),bWidth, bHeight), "Continue Journey")){
				Application.LoadLevel(PlayerPrefs.GetInt("level"));
			}
			GUI.enabled = true;
		
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2-(bHeight + bspace/2),bWidth, bHeight), "New Journey")){
				PlayerPrefs.SetInt("level", 1);
				Application.LoadLevel(1);
			}
		
			if (GUI.Button(new Rect(Screen.width/2-bWidth/2, Screen.height/2+bspace/2, bWidth, bHeight), "Chapter Select")){
				mScreen = 1;
			}
			
			if (GUI.Button(new Rect(Screen.width/2-bWidth/2, Screen.height/2 + bHeight + bspace/2+bspace*2, bWidth, bHeight), "Quit")){
				Application.Quit();
			}
		}
		
		if(mScreen == 1){
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2-(bHeight*3+ bspace*2 + bspace/2),bWidth, bHeight), "Chapter I")){
					PlayerPrefs.SetInt("level", 1);
					Application.LoadLevel(1);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2-(bHeight*2+ bspace + bspace/2),bWidth, bHeight), "Chapter II")){
					PlayerPrefs.SetInt("level", 2);
					Application.LoadLevel(2);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2-(bHeight + bspace/2),bWidth, bHeight), "Chapter III")){
					PlayerPrefs.SetInt("level", 3);
					Application.LoadLevel(3);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2+ bspace/2,bWidth, bHeight), "Chapter IV")){
					PlayerPrefs.SetInt("level", 4);
					Application.LoadLevel(4);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2+ bHeight + bspace + bspace/2,bWidth, bHeight), "Chapter V")){
					PlayerPrefs.SetInt("level", 5);
					Application.LoadLevel(5);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2+ bHeight * 2 + bspace * 2 + bspace/2,bWidth, bHeight), "Chapter VI")){
					PlayerPrefs.SetInt("level", 6);
					Application.LoadLevel(6);
				}
			
			if(GUI.Button(new Rect(Screen.width/2-bWidth/2,Screen.height/2+ bHeight*3 + bspace*4 + bspace/2,bWidth, bHeight), "Back")){
				mScreen = 0;
			}
		}
	}
}
