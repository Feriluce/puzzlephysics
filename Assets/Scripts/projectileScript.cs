using UnityEngine;
using System.Collections;

public class projectileScript : MonoBehaviour {
	
	public float force;
	public int damage;
	[HideInInspector]
	//public float instanceId;
	
	int platformDistance = 10;
	private float theTime;
	
	// Use this for initialization
	void Start () {
		Vector3 dir = transform.TransformDirection(Vector3.forward * force);
		rigidbody.AddForce(dir);
		theTime = Time.time;
		//instanceId = gameObject.GetInstanceID();
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time - theTime > 2){
			Destroy(gameObject);
		}
	}
	
	void OnCollisionEnter(Collision col) {
		
				Destroy(gameObject);
		
	}
	

	
}
