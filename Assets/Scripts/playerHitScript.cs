using UnityEngine;
//using System.Collections;
using System;

//I know this script is code duplication and bad, but it seems appropriate to have a seperate script for this,
//as monsters and objects doesn't have health.

public class playerHitScript : MonoBehaviour {
	
	public int maxHealth;
	[HideInInspector]
	public int currentHealth;
	public string plane = "front";
	int platDist = 10;					//Current distance between platforms. Change if distance changes.
	PlatformerController platcontrol;
	CharacterController controller;
	public float mass = 3.0f; // define the character mass
  	Vector3 impact = Vector3.zero;
	PlayerAnimator playerAnimator;
	public int meleeDamageTakenFromEnemies = 50;
	public float howOftenIsTheMeleeDamage = 0.5f;
	private float timeSinceLastCollision = 1.0f;
	private bool inMenu = false;
	
	
	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		platcontrol = GetComponent<PlatformerController>();
		controller = GetComponent<CharacterController>();
		playerAnimator = transform.GetComponentInChildren<PlayerAnimator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentHealth <= 0){
			currentHealth = maxHealth;
			platcontrol.OnDeath();
		}
		
		// apply the impact effect:
	    if (impact.magnitude > 0.2f){
	      controller.Move(impact * Time.deltaTime);
	    }
	    // impact energy goes by over time:
	    impact = Vector3.Lerp(impact, Vector3.zero, 10*Time.deltaTime);
		
		if (Input.GetKeyDown("escape")){
			if(!inMenu){
				Time.timeScale = 0.0f;
				inMenu = true;
			} else {
				Time.timeScale = 1.0f;
				inMenu = false;
			}
			
		}
	}
	
	void OnCollisionEnter(Collision col){
		foreach(ContactPoint contact in col.contacts){
			if(contact.otherCollider.CompareTag("teleportProjectile") && contact.otherCollider.gameObject.layer != 12){
				if(plane == "front" && !platcontrol.capsuleCheck(10)){
					transform.Translate(Vector3.forward*platDist, Space.World);
					platcontrol.planeOfExistence = plane = "back";
				} else if(!platcontrol.capsuleCheck(-10)){
					transform.Translate(Vector3.forward*(-platDist), Space.World);
					platcontrol.planeOfExistence = plane = "front";
				}
				playerAnimator.hitAnimation();
			}
			else														//this is so player doesn't shoot himself.
			if(contact.otherCollider.CompareTag("dmgProjectile") && contact.otherCollider.gameObject.layer != 12){
				projectileScript pScript = contact.otherCollider.GetComponent<projectileScript>();
				currentHealth -= pScript.damage;
				AddImpact(transform.position - contact.otherCollider.transform.position);
				playerAnimator.hitAnimation();
			}
			else
			if(Time.time - timeSinceLastCollision > howOftenIsTheMeleeDamage
				&& checkEnemy(contact.otherCollider.transform) && contact.otherCollider.gameObject.layer != 16){
				currentHealth -= meleeDamageTakenFromEnemies;
				AddImpact(transform.position - contact.otherCollider.transform.position);
				playerAnimator.hitAnimation();
			}
			//Debug.Log ("contact.otherCollider.gameObject.layer: " +contact.otherCollider.gameObject.layer);
		}
		
		
	}
	
	public void dealDamage(int howMuch, Vector3 otherColliderPosition, bool force){
			currentHealth -= howMuch;
			AddImpact(transform.position - otherColliderPosition);
			transform.position = new Vector3(transform.position.x, transform.position.y+0.8f, transform.position.z);
			if(force)
				playerAnimator.hitAnimationForce();
			else 
				playerAnimator.hitAnimation();
	}
	
	
	
	void OnGUI(){
		//GUI.Box(new Rect(Screen.width - 100, 0, 100, 25), currentHealth.ToString());
		GUI.HorizontalScrollbar(new Rect (Screen.width/2-100,Screen.height-80,200,20), 0, currentHealth,0, 100);
		
		
		if(inMenu){
			if(GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 - 33, 300, 66), "Quit to Menu")){
				Time.timeScale = 1.0f;
				Application.LoadLevel(8);
			}
		}
	}
	
	
	void AddImpact(Vector3 force){ // CharacterController version of AddForce
	    impact += 50*(force / mass);
	}
	

	bool checkEnemy(Transform trans){


		if(trans.GetComponent<teleportObjectScriptWithinParent>()) {
			timeSinceLastCollision = Time.time;
			return true;
		} else return false;

	}
	/*
	  	int n = 0;
		bool checkEnemy(Transform trans){
		n++;
		teleportObjectScriptWithinParent parentScript = null;
		Debug.Log ("testing against: " + trans.name);
		try{
			parentScript = trans.GetComponent<teleportObjectScriptWithinParent>();
		}
		catch( Exception e ){
			
		}
		
		if(n > 5){
			return false;	
		} else if(parentScript) {
			return true;
		} else {
			return checkEnemy(trans.parent);
		}

	}
	*/
}