using UnityEngine;
using System.Collections;

public class enemyScriptNew : MonoBehaviour
{
	/* Index of the enemy types
	 * 0 = Patroller
	 * 1 = Stretchy
	 * 2 = Stationary shooter
	 */
	public int enemyIndex = 0;
	public float speed = 0.0f;
	public float patrolDistance = 0.0f;
	public int shootingRange = 10;
	public GameObject projectile;
	public bool doesItLookAtYouWhilePatrolling = true;
	private int rotationSpeed = 1;
	public GameObject GameObjectToBeSpawnedInResponseToPlayerBeingOnTheOppositePlane = null;
	
	Vector3 patrolDir = Vector3.right;
	Vector3 moveDir;
	Vector3 startpos;
	GameObject player;
	float playerDist;
	[HideInInspector]
	public int currentPlane;
	int playerPlane;
	float fireRate = 1.0f;
	float nextFire = 0.0f;
	teleportObjectScript meshScript;
	GameObject shadow;
	
	CharacterController controller;
	CharacterMotor motor;
	private float initialY;
	// Use this for initialization
	void Start ()
	{
		currentPlane = Mathf.RoundToInt(transform.position.z);
		
		
		controller = GetComponent<CharacterController>();
		motor = GetComponent<CharacterMotor>();
		
		startpos = transform.position;
		player = GameObject.FindGameObjectWithTag("Player");
		
		meshScript = GetComponentInChildren<teleportObjectScript>();
		
		shadow = findChildWithTag("enShad");
		
		initialY = transform.position.y;
		//if(!meshScript)
			//Debug.Log ("THIS GUY DOES NOT HAVE toswp: "+transform.name);
	}
	
	// FIND CHILD WITH TAG
	GameObject findChildWithTag(string tagToFind) {
	    return findChildWithTag(tagToFind, this.gameObject);
	}
	GameObject findChildWithTag(string tagToFind, GameObject startingObject) {
	
	    Component[] childTransforms = startingObject.GetComponentsInChildren<Transform>();
	    foreach (Component thisComponent in childTransforms) { 
	       Transform thisTransform = thisComponent as Transform;
	       if (thisTransform.gameObject.tag == tagToFind) {
	         return thisTransform.gameObject as GameObject;
	       }
	    }
	    return null;
	}
	
	public float verticalVelocityControl = 20.0f;
	// Update is called once per frame
	void Update ()
	{
		if (transform.position.y < -40) {
			Destroy(gameObject);
		}
		if(controller && controller.velocity.y > verticalVelocityControl){
			//Debug.Log(controller.velocity.y);
			//controller.Move(new Vector3(controller.velocity.x, initialY, controller.velocity.z));
			//transform.position = new Vector3(transform.position.x, initialY, transform.position.z);
			motor.movement.velocity = new Vector3(controller.velocity.x, 0, controller.velocity.z);
			//controller.rigidbody.velocity = Vector3.zero;
			//controller.rigidbody.angularVelocity = Vector3.zero;
		}
		
		if(meshScript){
			//float oldZ = transform.position.z;
			//float z = 0;
			if(meshScript.plane < 1){
				//z = 10;
				transform.position = new Vector3(transform.position.x, transform.position.y, 10);
				if(shadow)
					shadow.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
			}
			else {
				transform.position = new Vector3(transform.position.x, transform.position.y, 0);
				if(shadow)
					shadow.transform.position = new Vector3(transform.position.x, transform.position.y, 10);
			}
			//transform.position = new Vector3(transform.position.x, transform.position.y, z);
			//Debug.Log("MOVING "+transform.name+" from z: " + oldZ + ", to new z: "+transform.position.z);
			
		}
		switch(enemyIndex){
		case 0:
			patrol();
			break;
		case 1:
			patrol();
			break;
		case 2:
			shoot();
			break;
		case 3:
			chase();
			shoot();
			break;
		}
		
		//transform.position = new Vector3(transform.position.x, transform.position.y, currentPlane);
	}
	
	void LateUpdate(){
		
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit){
		if(hit.gameObject.layer == 16){
			Destroy(transform.gameObject);
		}
		
		switch(enemyIndex){
		case 0:
			patrolCollision(hit);
			break;
		case 1:
			patrolCollision(hit);
			break;
		case 2:
			break;
		}
	}
	
	void patrol ()
	{
		Vector3 tempvect = new Vector3(transform.position.x, transform.position.y, currentPlane);
		transform.position = tempvect;
		
		moveDir = transform.TransformDirection(patrolDir);
		moveDir *= speed;
		//controller.Move(moveDir * Time.deltaTime);
		controller.Move(moveDir * Time.deltaTime);
		//transform.position = new Vector3(transform.position.x, transform.position.y+0.15f, transform.position.z);
		
		if(transform.position.x > startpos.x + patrolDistance){
			patrolDir = Vector3.left;
		}
		
		if(transform.position.x < startpos.x){
			patrolDir = Vector3.right;
		}
		
		
	
	}
	
	//private const float TELE_INTERVAL = 0.05f;
	//private float teleInterval = TELE_INTERVAL;
	void chase ()
	{
		Vector3 playerPos = player.transform.position;
		moveDir = playerPos - transform.position;
		moveDir *= speed / moveDir.magnitude;
		controller.Move(moveDir * Time.deltaTime);
		if (GameObjectToBeSpawnedInResponseToPlayerBeingOnTheOppositePlane/* && teleInterval <= 0*/ && Mathf.Abs(playerPos.z - transform.position.z) > 5) {
			GameObject.Instantiate(GameObjectToBeSpawnedInResponseToPlayerBeingOnTheOppositePlane, transform.position, transform.rotation);
			//teleInterval = TELE_INTERVAL;
		/*} else {
			teleInterval -= Time.deltaTime;*/
		}
	}
	
	void patrolCollision (ControllerColliderHit hit)
	{
		//if(!(hit.moveDirection.y > -1.1f && hit.moveDirection.y < -0.9f) && hit.moveDirection.y == -1){// || hit.transform.name.Equals("icosphere")){//-0.2f
		//	return;
		//}
		//Debug.Log("collided: hit.moveDirection.y: " + hit.moveDirection.y);
		if(hit.moveDirection.y != -1 && transform.gameObject.layer != 16 && !hit.collider.gameObject.tag.Equals("Player"))
			patrolDir.x = -patrolDir.x;
	}
	
	void shoot()
	{
		playerDist = Mathf.Abs(player.transform.position.x - transform.position.x);
		//currentPlane = Mathf.RoundToInt(transform.position.z);
		playerPlane = Mathf.RoundToInt(player.transform.position.z);
		
		if(currentPlane <= playerPlane+2 && currentPlane >= playerPlane-2){
			
			if(playerDist < shootingRange){
				if(projectile && Time.time > nextFire){
					nextFire = Time.time + fireRate;
			
					//transform.LookAt(player.transform);
					//instant LookAt is ugly. Used Lerp instead.
					transform.rotation = Quaternion.Slerp(transform.rotation,
													   Quaternion.LookRotation(player.transform.position - transform.position) , 
													   rotationSpeed*10 * Time.deltaTime);
					//Still, we need to figure out a better way to spawn the projectile.
					//It should always spawn on the same Z coordinate, and not affect the shotoer.
					

					//Vector3 pos = transform.TransformPoint(Vector3.forward);//Vector3(target1.position.x, transform.position.y, transform.position.z);// + Vector3(transform.position.x, transform.position.y, target1.position.z);
					
					
					//GameObject proj = Instantiate(projectile, transform.position, Quaternion.LookRotation(player.transform.position - transform.position)) as GameObject;//transform.rotation);
					Quaternion q = player.transform.rotation;
					q.x *= -1;
					GameObject proj = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y+1.2f, transform.position.z), Quaternion.LookRotation(player.transform.position - transform.position)) as GameObject;//transform.rotation);
					
					if(collider)
					Physics.IgnoreCollision(proj.collider, collider);
					
					if(collider)
					if(collider.GetComponentInChildren<MeshCollider>()){
						Physics.IgnoreCollision(proj.collider, collider.GetComponentInChildren<MeshCollider>());
					}
					else if(collider.GetComponentInChildren<BoxCollider>()){
						Physics.IgnoreCollision(proj.collider, collider.GetComponentInChildren<BoxCollider>());
					}
					
					
				}
			}
		}
		
		//TODO: instead of this, we should rotate the enemy to its neutral position. 
		//I've added this because without it, the enemy remains facing in the direction it last shot in.
		if(doesItLookAtYouWhilePatrolling){//Keeps staring at you creepily
				transform.rotation = Quaternion.Slerp(transform.rotation,
													   Quaternion.LookRotation(player.transform.position - transform.position) , 
													   rotationSpeed * Time.deltaTime);
		
		}
	}
}

