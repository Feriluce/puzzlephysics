using UnityEngine;
using System.Collections;

public class teleportObjectScriptWithinParent : MonoBehaviour {
	[HideInInspector]
	public int plane;
	[HideInInspector]
	int platDist = 10;					//Current distance between platforms. Change if distance changes.
	
	private enemyScriptNew enemyScript;
	private EnemyScript enemyScriptOld;
	private teleportObjectScript parentScript;
	
	private CharacterController charContr;
	//private CharacterMotor charMotor;
	
	
	private GameObject debugSphere;
	private GameObject debugSphere2;
	private GameObject debugResource;
	
	private	GameObject shadow;
	private Shader shader4;
	private ApplyCustomShader shadowMesh;
	
	GameObject player;
	
	// Use this for initialization
	void Start () {
		
		
		//get first parent with teleportobjectscript
		parentScript = getMasterParent(transform.parent);
		
		if(transform.position.z < 5 ){
			plane = 1;
		}
		else {
			plane = 0;
		}
		//plane = parentScript.plane;
		
		charContr = (CharacterController)parentScript.transform.GetComponent(typeof(CharacterController));
		//charMotor = (CharacterMotor)parentScript.transform.GetComponent(typeof(CharacterMotor));

		//Here I'm accessing a parameter from a JavaScript script from within C#. Yeah!
		//the js script must be placed in the puzzlephysics/Assete/Plugins folder or else it won't be compiled in time for the C# scripts
		enemyScript = parentScript.transform.GetComponent<enemyScriptNew>();
		if(!enemyScript){
			enemyScriptOld = parentScript.transform.GetComponent<EnemyScript>();
		}
		
		player = GameObject.FindGameObjectWithTag("Player");
		
		debugResource = (GameObject)Resources.Load("debug Sphere");
		//debugResource.collider.isTrigger = true;
		//debugResource.collider.enabled = false;
		
		//parentTemp = GameObject.FindGameObjectWithTag("stretchy-enemy[0]1");
		
		shadow = findChildWithTag("enShad", parentScript.transform.gameObject);
		
		shader4 = Shader.Find( "AlphaSelfIllum" );
		
		if(shadow && shadow.transform.GetComponentInChildren<ApplyCustomShader>())
			shadowMesh = shadow.transform.GetComponentInChildren<ApplyCustomShader>();
	}
	
	// FIND CHILD WITH TAG
	GameObject findChildWithTag(string tagToFind) {
	    return findChildWithTag(tagToFind, this.gameObject);
	}
	GameObject findChildWithTag(string tagToFind, GameObject startingObject) {
	
	    Component[] childTransforms = startingObject.GetComponentsInChildren<Transform>();
	    foreach (Component thisComponent in childTransforms) { 
	       Transform thisTransform = thisComponent as Transform;
	       if (thisTransform.gameObject.tag == tagToFind) {
	         return thisTransform.gameObject as GameObject;
	       }
	    }
	    return null;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log("transform becoming 0: "+transform.name);
		transform.localPosition = new Vector3(0, 0, 0);
		
		if(shadowMesh)
		if(Mathf.Abs(player.transform.position.z - parentScript.transform.position.z) < 5){
			//shadowMesh.restoreToCustomShader();
			shadowMesh.transform.renderer.material.color = new Color(shadowMesh.transform.renderer.material.color.r, shadowMesh.transform.renderer.material.color.g, shadowMesh.transform.renderer.material.color.b, 0.2f);
			//shadowMesh.applicableAlpha = 0.2f;
		} else{
			shadowMesh.transform.renderer.material.color = new Color(shadowMesh.transform.renderer.material.color.r, shadowMesh.transform.renderer.material.color.g, shadowMesh.transform.renderer.material.color.b, 0.0f);
			//shadowMesh.applicableAlpha = 0.0f;
		}
	}
	
	private int failsafe = 0;
	teleportObjectScript getMasterParent(Transform thees){
		failsafe++;
		//Debug.Log ("failsafe (0) is: "+failsafe);
		//Debug.Log("(has no parent?) thees is: "+thees.name);
		//Debug.Log("; o rly, tag: " + thees.tag);
		//Transform parent = (thees.parent ? thees.parent : thees);
		Transform parent = thees;
		/*
		try{
			parent = thees.parent;
		}
		catch(Exception e){
			parent = thees;
		}
		*/
		if(parent.GetComponent<teleportObjectScript>()!=null)
			parentScript = parent.GetComponent<teleportObjectScript>();
		if(failsafe > 5){
			//Debug.Log();
			failsafe = 0;
			throw new System.InvalidOperationException("This mesh renderer does not have a parent with teleportObjectScript on it: "+transform.name);
			//return null;//this
		} else
			if(parentScript && parentScript.redLifeTime >-1){
				failsafe = 0;

				return parentScript;
			} else
				return getMasterParent(parent.parent);
	}
	
	

	void OnCollisionEnter(Collision col){

		foreach(ContactPoint contact in col.contacts){
			/*
			if (!(contact.normal.y > -0.8f && contact.normal.y < 0.8f)){
				Debug.Log ("For: " + contact.otherCollider.transform.name+"; normal.y: -1< " +contact.normal.y+" <1");
				Debug.Log ("Ignoring collision with: " + contact.otherCollider.transform.name);
				Physics.IgnoreCollision(contact.thisCollider, contact.otherCollider);
			}
			else*/
			if(contact.otherCollider.CompareTag("teleportProjectile") 
				&& parentScript.transform.CompareTag("teleportObject")){
				
				
					if(plane == 1){
						bool hit = sphereCheck(10);
						if(!hit){//Debug.Log("COLLISION WITH PROJECTILE 2");
							if(enemyScript){
								enemyScript.currentPlane = 10;
								parentScript.plane = plane = 0;
								//Debug.Log ("enemyScript.currentPlane: " + enemyScript.currentPlane);
								//charContr.transform.position = new Vector3(charContr.transform.position.x, charContr.transform.position.y, charContr.transform.position.z+platDist);
								//charContr.transform.Translate(Vector3.forward*platDist, Space.World);
								//charContr.center = new Vector3(charContr.center.x, charContr.center.y, charContr.transform.position.z);
								//transform.Translate(Vector3.forward*(platDist), Space.World);
								//transform.parent.transform.Translate(Vector3.forward*(platDist), Space.World);
								//transform.parent.parent.transform.Translate(Vector3.forward*(platDist), Space.World);
							} //else{
								//charContr.transform.position = new Vector3(charContr.transform.position.x, charContr.transform.position.y, charContr.transform.position.z+platDist);
								//charContr.transform.Translate(Vector3.forward*platDist, Space.World);
								//charContr.center = new Vector3(charContr.center.x, charContr.center.y, charContr.transform.position.z);
								//transform.Translate(Vector3.forward*(platDist), Space.World);
								//transform.parent.transform.Translate(Vector3.forward*(platDist), Space.World);
								//transform.parent.parent.transform.Translate(Vector3.forward*(platDist), Space.World);
							//}
							else if(enemyScriptOld){
								parentScript.plane = plane = 0;
								enemyScriptOld.currentPlane = 10;
							}
							
							//Debug.Log("z of: " + parentScript.name + "; changed to: " + parentScript.transform.position.z );
							//Debug.Log("parentScript.plane: " + parentScript.plane );
					}
						
					} else {
						bool hit = sphereCheck(-10);
						if(!hit){//Debug.Log("COLLISION WITH PROJECTILE 3");
							if(enemyScript){
								parentScript.plane = plane = 1;
								enemyScript.currentPlane = 0;
								//Debug.Log ("enemyScript.currentPlane: " + enemyScript.currentPlane);
								//charContr.transform.position = new Vector3(charContr.transform.position.x, charContr.transform.position.y, charContr.transform.position.z-platDist);
								//charContr.transform.Translate(Vector3.forward*(-platDist), Space.World);
								//charContr.center = new Vector3(charContr.center.x, charContr.center.y, charContr.transform.position.z);
								//transform.Translate(Vector3.forward*(-platDist), Space.World);
								//transform.parent.transform.Translate(Vector3.forward*(-platDist), Space.World);
								//transform.parent.parent.transform.Translate(Vector3.forward*(-platDist), Space.World);
							} //else{
								//charContr.transform.position = new Vector3(charContr.transform.position.x, charContr.transform.position.y, charContr.transform.position.z-platDist);
								//charContr.transform.Translate(Vector3.forward*(-platDist), Space.World);
								//charContr.center = new Vector3(charContr.center.x, charContr.center.y, charContr.transform.position.z);
								//transform.Translate(Vector3.forward*(-platDist), Space.World);
								//transform.parent.transform.Translate(Vector3.forward*(-platDist), Space.World);
								//transform.parent.parent.transform.Translate(Vector3.forward*(-platDist), Space.World);
							//}
							else if(enemyScriptOld){
								parentScript.plane = plane = 1;
								enemyScriptOld.currentPlane = 0;
							}
							
							//Debug.Log("z of: " + parentScript.name + "; changed to: " + parentScript.transform.position.z );
							//Debug.Log("parentScript.plane: " + parentScript.plane );
						}
						
					}

			}
		}
	}
	
	IEnumerator restoreShadowColor(){
	  yield return new WaitForSeconds(redLifeTime/1.5f);

	  
	  shadowMesh.transform.renderer.material.color = Color.black;
	  shadowMesh.restoreToCustomShader();
	  //shadowMesh.transform.renderer.material.color.a = 0.2;

      //StartCoroutine("restoreShadowColor");
	}
	
	public float redLifeTime = 1;
	bool sphereCheck(float zWard){
		Rigidbody rigidBody = (Rigidbody)GetComponent(typeof(Rigidbody));
		if(!rigidBody){//then there is a characterController instead
			return capsuleCheck(zWard);
		} 
    
		//float radius = (rigidbody.collider.bounds.size.x > rigidbody.collider.bounds.size.y ? rigidbody.collider.bounds.size.x/2 : rigidbody.collider.bounds.size.y/2 );
		float radius = rigidbody.collider.bounds.size.y/2 ;
		Vector3 position2 = transform.position + new Vector3(0,rigidbody.collider.bounds.size.y/2,zWard);
		int mask = 1 << 11;
		mask = ~mask;
		
		if(Physics.CheckSphere(position2, radius, mask)){
			
			
			if(shadowMesh){
				shadowMesh.transform.renderer.material.shader = shader4;
				shadowMesh.transform.renderer.material.color = new Color(Color.red.r, Color.red.g, Color.red.b, 1);
				
				StartCoroutine("restoreShadowColor");
			} else{
				debugSphere = Instantiate(debugResource, position2, transform.rotation) as GameObject;
				//debugSphere.transform.position = position2;
				debugSphere.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				Destroy (debugSphere, redLifeTime);
			}
			 
			
			//Debug.Log("___________________ [rigidbody] You can't teleport object to: " + position2.z + "; object is on: " + transform.position.z);
			return true;
		}
		else return false;

	}

	
	bool capsuleCheck(float zWard){// capsule without a cast/sweep. just checks the destination: this is what we really need!
			
			float height = transform.collider.bounds.extents.y;//charContr.height;
		    Vector3 p1 = transform.position + charContr.center +//transform.collider.bounds.center + 
		                	Vector3.up * (-height/2 + height/9)
							+ new Vector3(0, 0, zWard);
		    Vector3 p2 = p1 + Vector3.up * (height - height/9);// + new Vector3(0, height/2, 0);
			
			float radius = charContr.radius;//transform.collider.bounds.extents.x;
			int mask = 1 << 11;
			mask = ~mask;
	
			if(Physics.CheckCapsule(p1, p2, radius, mask)){
				
			
			if(shadowMesh){
				shadowMesh.transform.renderer.material.shader = shader4;
				shadowMesh.transform.renderer.material.color = new Color(Color.red.r, Color.red.g, Color.red.b, 1);
				StartCoroutine("restoreShadowColor");
			} else{
				//debugSphere = Instantiate(debugResource, p1, transform.rotation) as GameObject;
				debugSphere = Instantiate(debugResource, p1, transform.rotation) as GameObject;
				//debugSphere.transform.position = p1;
				debugSphere.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				
				debugSphere2 = Instantiate(debugResource, p2, transform.rotation) as GameObject;
				//debugSphere2.transform.position = p2;
				debugSphere2.transform.localScale = new Vector3(radius*2, radius*2, radius*2);
				
				Destroy (debugSphere, redLifeTime);
				Destroy (debugSphere2, redLifeTime);
			}
			
			
			//Debug.Log("___________________ [character controller] You can't teleport target to: " + p1.z + "; object is on: " + transform.position.z);
			return true;
		}
		else return false;
	}
}