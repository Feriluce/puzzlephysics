using UnityEngine;
using System.Collections;

public class nextLevelScript : MonoBehaviour {
	
	
	int nxtLvl;
		
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider col) {
		if(col.gameObject.CompareTag("Player")){
			nxtLvl = Application.loadedLevel+1;
			
			if(nxtLvl < Application.levelCount){
				PlayerPrefs.SetInt("level", nxtLvl);
				Application.LoadLevel(nxtLvl);
			}
		}
	}
}
