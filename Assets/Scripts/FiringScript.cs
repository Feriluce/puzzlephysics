using UnityEngine;
using System.Collections;

public class FiringScript : MonoBehaviour {
	
	public GameObject projectile;
	
	// Use this for initialization
	GameObject player;
	PlatformerController playerScript;
	PlayerAnimator playerAnimator;
	void Start(){
		player = GameObject.FindWithTag("Player");
		playerScript = (PlatformerController)player
					   .GetComponent("PlatformerController");
		playerAnimator = player.GetComponentInChildren<PlayerAnimator>();
		
		//proj = new GameObject();
	}
	
	
	float nextFire = 0;
	float fireRate = 0.5f;
	// Update is called once per frame
	void Update () {
		
		//playerScript.get
		
		//TODO: fix the slow projectile bug by transforming based on this direction:
		//if(playerScript.GetDirection().x == -1) //it's always either 1 or -1 
			//shoot projectile left
		//if(playerScript.GetDirection().x == 1)
			//shoot projectile right
		
		if(!playerScript.dontShoot && Input.GetButtonDown("Fire") && Time.time > nextFire){
			
			playerAnimator.fireAnimation();
			//nextFire = Time.time + fireRate;

			StartCoroutine(setTimeout(0.0f));
			//fireProjectile();
		}
		

	}
	
	//GameObject proj;
	void fireProjectile(){
		nextFire = Time.time + fireRate;
		
		Vector3 pos = player.transform.TransformPoint(new Vector3(0,0,0.01f)) + new Vector3(player.transform.forward.x*1.5f, -player.transform.forward.y/2, 0);
		//proj = Instantiate(projectile, pos, transform.rotation) as GameObject;
		GameObject proj = Instantiate(projectile, pos, Quaternion.LookRotation(new Vector3(player.transform.forward.x,0,0))) as GameObject;
		proj.layer = 12;
		
		//proj.AddComponent("Transform");
		//proj.transform.parent = player.transform;
		//proj.transform.localScale = Vector3.one;
	}
	
	bool canStart = true;
	IEnumerator setTimeout(float t){
		yield return new WaitForSeconds(t);
		fireProjectile();
	}
}
