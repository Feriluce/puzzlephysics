using UnityEngine;
using System.Collections;

public class spawnScript : MonoBehaviour {
	
	PlatformerController platcontrol;
	
	// Use this for initialization
	void Start () {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		platcontrol = player.GetComponent<PlatformerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI (){
		if(GUI.Button(new Rect(10,10,100,25), "Load Checkpoint")){
			/*
			int level = PlayerPrefs.GetInt("level");
			float playerX = PlayerPrefs.GetFloat("playerX");
			float playerY = PlayerPrefs.GetFloat("playerY");
			float playerZ = PlayerPrefs.GetFloat("playerZ");
			
			Vector3 pos = new Vector3(playerX, playerY, playerZ);
			transform.position = pos;
			*/
			platcontrol.Spawn();
		}
	}
}
