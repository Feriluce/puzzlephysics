using UnityEngine;
using System.Collections;

public class checkpointScript : MonoBehaviour {
	
	PlatformerController platcontrol;
	
	// Use this for initialization
	void Start () {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		platcontrol = player.GetComponent<PlatformerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter (Collider col) {
		if(col.gameObject.CompareTag("Player")){
			PlayerPrefs.SetInt("level", Application.loadedLevel);
			PlayerPrefs.SetFloat("checkX", transform.position.x);
			PlayerPrefs.SetFloat("checkY", transform.position.y);
			PlayerPrefs.SetFloat("checkZ", transform.position.z);
			PlayerPrefs.Save ();
			
			platcontrol.spawnPoint = transform;
		}
	}
}
