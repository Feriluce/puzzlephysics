using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour
{
	private const float SPAWN_TIME_DEF = 5.0f;
	
  // Spawn location
  public Vector3 spawnDisplacement = new Vector3(0, 0, 0);
  // Spawn timer (seconds)
  public float spawnTimer = SPAWN_TIME_DEF;
	public bool left = false;
  private float spawnTimeRemaining = SPAWN_TIME_DEF;

  // The zombie to spawn
  public GameObject spawneePrefab = null;

  void Awake()
  {
    spawnTimeRemaining = spawnTimer;
  }
	
	
  public bool startSpawning = true;
  void FixedUpdate()
  {
	if(startSpawning)
	{
	    spawnTimeRemaining -= Time.deltaTime;
	
	    if (spawnTimeRemaining < 0.0f)
	    {
	      //GameObject s = (GameObject)
					GameObject.Instantiate(spawneePrefab, transform.position + spawnDisplacement, (left ? Quaternion.Euler(0, 180, 0) : Quaternion.Euler(0, 0, 0)));
		  //s.tag = "friendly";
				
	      spawnTimeRemaining = spawnTimer;
	    }
	}
  }
}