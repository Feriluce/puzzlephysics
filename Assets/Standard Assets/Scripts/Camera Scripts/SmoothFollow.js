/*
This camera smoothes out rotation around the y-axis and height.
Horizontal Distance to the target is always fixed.

There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

For every of those smoothed values we calculate the wanted value and the current value.
Then we smooth it using the Lerp function.
Then we apply the smoothed values to the transform's position.
*/

// The target we are following
var target : Transform;
// The distance in the x-z plane to the target
public var Camera_Distance = 30;
var distance = Camera_Distance;
// the height we want the camera to be above the target
var height = 5.0;
// How much we 
var heightDamping = 2.0;
var KeyDown = false;
private var rotationDamping = 0.0;
private var factor = -0.15;
var bg:GameObject;
var blur:Transform;
var bg_plat:GameObject;
// Place the script in the Camera-Control group in the component menu
@script AddComponentMenu("Camera-Control/Smooth Follow")
private var _bg_y:float;
//var setObject : GameObject;
//var script : PlatformerController;
//var platform_fg : GameObject[];
var initial_z = 0.0;
function Start() {
    bg = GameObject.FindWithTag("background"); //If you must...
	initial_z = bg.transform.position.z;
    //setObject = GameObject.Find("Character (Lerpz)"); //as a last resort
    //script = setObject.GetComponent("PlatformerController");
    //var script: PlatformerController = GameObject.Find( "Mia" ).GetComponent(PlatformerController); 
    
    bg_plat = GameObject.Find("Platform_Background");
    blur = transform.FindChild("Blur Plane");
    
    _bg_y = bg.transform.position.y;
}


function LateUpdate () {
/*
	if(Input.GetButton ("Cheat")){
		if (!KeyDown) {
			//heightDamping = (heightDamping < 0.5 ? 1.0 : 0.0);
			transform.rotation.y = 180;
			KeyDown = true;
		}
	}else
	if (KeyDown) {
			KeyDown = false; //turns off the loop
			transform.rotation.y = 5;
	}
*/	
	// Early out if we don't have a target
	if (!target)
		return;
	
	// Calculate the current rotation angles
	var wantedRotationAngle = target.eulerAngles.y;
	var wantedHeight = target.position.y + height;
		
	var currentRotationAngle = transform.eulerAngles.y;
	var currentHeight = transform.position.y;
	
	// Damp the rotation around the y-axis
	currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);


	

	// Damp the height
	currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);

	// Convert the angle into a rotation
	var currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
	

	// Set the position of the camera on the x-z plane to:
	// distance meters behind the target
	transform.position = target.position;
	//transform.position -= currentRotation * Vector3.forward * (distance );
/*	
	if(script && script.planeOfExistence == "front"){
		transform.position -= currentRotation * Vector3.forward * (distance - 10);
		changeAlphaOfObjectList(platform_fg, 1);
	}
	else if(script && script.planeOfExistence == "back"){
		transform.position -= currentRotation * Vector3.forward * (distance );
		changeAlphaOfObjectList(platform_fg, 0.5);
	}
	else
		transform.position -= currentRotation * Vector3.forward * (distance );
*/
	transform.position -= currentRotation * Vector3.forward * (distance );

	// Set the height of the camera
	transform.position.y = currentHeight;
	
	// Always look at the target
	transform.LookAt (target);
	
	//bg.transform.LookAt(transform);
	//bg.transform.position.z = blur.position.z;
	bg.transform.rotation = blur.rotation;
	
	bg.transform.rotation.x += factor * bg.transform.rotation.x;
	bg.transform.rotation.z += factor * bg.transform.rotation.z;
	//bg.transform.position.y = _bg_y;
	//bg_plat.transform.rotation.x = transform.rotation.x;

}
/*
function changeAlphaOfObjectList(list, alpha){
	for(var i in list){
		i.renderer.material.color.a = Mathf.Lerp(i.renderer.material.color.a,alpha,Time.deltaTime*2);
		//i.renderer.material.color.a = alpha;
	}

}
*/