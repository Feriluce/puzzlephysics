#pragma strict


private var platScript:PlatformerController;
private var player:GameObject;
public var shootanim : AnimationState;
public var hurtanim : AnimationState;
function Start () {
	player = GameObject.FindWithTag("Player");
	
	//platScript = transform.parent.gameObject.GetComponent("PlatformerController");
	platScript = player.GetComponent("PlatformerController");
	
	animation["walk"].speed = 2;
	animation["run"].speed = 2;
 	animation["idle"].speed = 1;
 	animation["hurt"].speed = 3;
 	animation["jump"].speed = 3;
 	animation["shoot"].speed = 3;
 	animation["walk"].wrapMode = WrapMode.Loop;
 	animation["idle"].wrapMode = WrapMode.Loop;
 	animation["hurt"].wrapMode = WrapMode.Once;
 	animation["jump"].wrapMode = WrapMode.Once;
 	//animation["jump"].time = 1.5;//animation["jump"].length/4;
 	//animation["jump"].blendMode = AnimationBlendMode.;
 	//animation["shoot"].wrapMode = WrapMode.Once;

   	//var rootBone = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1"); 
	//var rootBone = GameObject.FindGameObjectWithTag("fireBone").transform; 
	var rootBones:GameObject[] = GameObject.FindGameObjectsWithTag("fireBone"); 

   	shootanim = animation["shoot"];
 	shootanim.layer = 2;
 	shootanim.blendMode = AnimationBlendMode.Blend;
 	shootanim.wrapMode = WrapMode.Once;//WrapMode.ClampForever;
 	
 	shootanim.enabled = false;
 	shootanim.weight = 1.0;
 	for(var bone:GameObject in rootBones){
 		shootanim.AddMixingTransform(bone.transform); 
 	}
 	
 	var hitBones:GameObject[] = GameObject.FindGameObjectsWithTag("hitBone"); 
 	
 	hurtanim = animation["hurt"];
 	hurtanim.layer = 2;
 	hurtanim.blendMode = AnimationBlendMode.Blend;
 	hurtanim.wrapMode = WrapMode.Once;//WrapMode.ClampForever;
 	
 	hurtanim.enabled = false;
 	hurtanim.weight = 1.0;
 	for(var bone:GameObject in hitBones){
 		hurtanim.AddMixingTransform(bone.transform); 
 	}
	
 	animation["walk"].layer = 1;
 	animation["run"].layer = 1;
 	animation["idle"].layer = 1;
 	animation["jump"].layer = 1;
 	animation["walk"].weight = 0.1;
 	animation["run"].weight = 0.1;
	
}

//public var canFireAnim = false;
function Update () {
	//if(animation["jump"].time < 1.5){
	//	animation["jump"].time = 1.5;
	//}
		
	//I should do the rest of the animations in the platformercontroller script 
	//because I already handle the input and movement there. 
	
	//animation.Play("idle");
	/*
	if(canFireAnim){
		//canFireAnim = false;
		fireAnimation();
	} else 
	*/
	
	//if(! animation.IsPlaying("shoot")){

		//if(platScript.IsJumping() && !platScript.ReachedApex()){
		if(platScript.IsJumping() && !platScript.ReachedApex() && platScript.GetHangTime()>0 && platScript.GetHangTime()< 0.4){
			//animation["jump"].time = animation["jump"].length/4;
			animation.CrossFade("jump");
			
			//animation.Play("jump");
			//animation.wrapMode = WrapMode.Once;
			//animation.wrapMode = WrapMode.ClampForever;
		}
		else
		if(platScript.GetSpeed() < 1){
	
			//animation.Play("idle");
			animation.CrossFade("idle");
			//animation.wrapMode = WrapMode.Loop;
		}
		else if(platScript.GetSpeed() < 8){
				animation.CrossFade("walk");
				//animation.wrapMode = WrapMode.Loop;
				//animation.Play("walk");
			}
		else{
				//animation.CrossFade("run");
				animation.CrossFade("run");// TODO: change this to RUN
				//animation.wrapMode = WrapMode.Loop;
				//animation.Play("run");
			}
	
	
	//}
		
	
		
}

public function fireAnimation(){
		//shootanim.normalizedTime = 0.4;//0 is start, 1 is end.
	  	//shootanim.enabled = true;
		animation.Play("shoot");
		

		//print("shot");
}

public function hitAnimation(){

		animation.CrossFade("hurt");

}

public function hitAnimationForce(){

		animation.Play("hurt");

}