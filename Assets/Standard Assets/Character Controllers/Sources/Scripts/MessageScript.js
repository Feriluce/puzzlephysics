#pragma strict

public var unlockSwitchPlane : boolean = false;
public var unlockShoot : boolean = false;
public var unlockMovePlane : boolean = false;
public var objectsToEnable : GameObject[];
private var player:GameObject;
private var script:PlatformerController;
function Start () {

	player = GameObject.FindGameObjectWithTag("Player"); 
 	
 	script = player.GetComponent("PlatformerController");
}

function Update () {
	
}

function OnTriggerEnter (col:Collider) {
		if(col.gameObject.CompareTag("Player")){
			
			if(unlockSwitchPlane)
				script.dontSwitch = false;
			else if(unlockShoot)
				script.dontShoot = false;
			else if(unlockMovePlane)
				script.dontPlane = false;
				
			for(var i=0; i<objectsToEnable.length; i++){
				objectsToEnable[i].active = true;
				
			}
			
		}
}

function OnTriggerExit (col:Collider) {
		if(col.gameObject.CompareTag("Player")){
			
			for(var i=0; i<objectsToEnable.length; i++){
				objectsToEnable[i].active = false;
				
			}
			
		}
}