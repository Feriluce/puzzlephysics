//#pragma strict

var textColorR:float=255.0;

var textColorG:float=155.0;

var textColorB:float=0.0;

var textColorA:float=1.0;

var enabledAtStartup:boolean=true;

private var componentGUIText;

function Start(){

    componentGUIText=GetComponent(GUIText);

	//componentGUIText.material = new Material();
    componentGUIText.material.color=Color(textColorR/255,textColorG/255,textColorB/255,textColorA);

}

function Update(){

    componentGUIText.enabled=enabledAtStartup;

    Destroy(this);

}