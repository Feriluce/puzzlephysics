#pragma strict

var chMotor: CharacterMotor;

function Start(){
    chMotor = GetComponent(CharacterMotor);
}

function SetGravity(g: float){
    chMotor.movement.gravity = g;
}