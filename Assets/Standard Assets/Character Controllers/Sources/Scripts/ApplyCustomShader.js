#pragma strict


public var customShaderName:String;
public var applicableAlpha:Number;
function Start () {

	renderer.material.shader = Shader.Find( customShaderName );
	if(renderer.material.color.a)
		renderer.material.color.a = applicableAlpha;
}

function Update () {

}

public function restoreToCustomShader(){
	renderer.material.shader = Shader.Find( customShaderName );
	if(renderer.material.color.a)
		renderer.material.color.a = applicableAlpha;

}