#pragma strict

public var target:GameObject;

function Start () {
	//Debug.Log("setting target to disabled");
	target.gameObject.SetActiveRecursively(false);
}

function Update () {

}

private var once:boolean = true;
function OnTriggerEnter (other : Collider) {
	if(once && other.tag == "Player"){
    	target.gameObject.SetActiveRecursively(true);
    	once = false;
    }
}

/*
function FindGameObjectsWithLayer (layer : int) : GameObject[] {
    var goArray = FindObjectsOfType(GameObject);
    var goList = new System.Collections.Generic.List.<GameObject>();
    for (var i = 0; i < goArray.Length; i++) {
       if (goArray[i].layer == layer) {
         goList.Add(goArray[i]);
       }
    }
    if (goList.Count == 0) {
       return null;
    }
    return goList.ToArray();
}
*/