#pragma strict

public var boss:GameObject;
public var player:GameObject;
private var playerScript:PlatformerController;
function Start () {
	//Debug.Log("setting boss to disabled");
	boss.gameObject.SetActiveRecursively(false);
	playerScript = player.GetComponent("PlatformerController");
}

function Update () {

}


private var once:boolean = true;
function OnTriggerEnter (other : Collider) {
	if(once && other.tag == "Player"){
    	boss.gameObject.SetActiveRecursively(true);
    	//Debug.Log("Setting player fall distance from " + playerScript.fallDistance);
    	playerScript.fallDistance *=3;
    	//Debug.Log(" to: " + playerScript.fallDistance);
    	once = false;
    }
}

/*
function FindGameObjectsWithLayer (layer : int) : GameObject[] {
    var goArray = FindObjectsOfType(GameObject);
    var goList = new System.Collections.Generic.List.<GameObject>();
    for (var i = 0; i < goArray.Length; i++) {
       if (goArray[i].layer == layer) {
         goList.Add(goArray[i]);
       }
    }
    if (goList.Count == 0) {
       return null;
    }
    return goList.ToArray();
}
*/