


This is a quick and ugly dump for the parameters you guys need to keep in mind when you do level design.
I'm not working directly on this, but whenever I come across something, I'll post it here.

If objects have (in their inspector):

> "Teleport Object Script": 
	-set the "Plane" variable to "front" (without quotes) or "back" according to which platform is the object placed on.
	-"Red Life Time" is the amount (in seconds) before the red bubbles disappear (the ones that pop up on the opposite plane if you can't teleport something (or yourself) because there's an obstacle). It doesn't have to be an integer, it can also be "1.32".

> "Enemy Script": 
	-  pretty self explanatory (there are also more details in one of the commits, not looking for them right now)

> "Platformer Controller" on Mia (the player character):
	- lots of parameters you can tweak here, will explain later... Or just play around yourself.

>


_______


Remember: If you want to stretch a bridge arc:
		-Create a new material (in the Project tab) on which you add the square 'Wall1' texture.
		-Then to your bridge duplicate, select the bridge arc object, and replace the "bridge-upper" material, with your new material.
		- in case the texture is at a wrong angle, make a copy of wall1.jpg, open it in photoship, rotate 90 degrees, update your material.

Also remember: cubes and enemies don't have to be placed in the foreground/background plane -- BUT they must be told which Plane they are on, in the "Teleport Object Script" component in the inspector: either "back" or "front".
		- just place new ones next to where the rest of them are.

No more need to have invisible platforms (we still use the invisible wall though!). 
I added colliders to the bridges. 
You can copy those, or you can add your own in the same manner.
HOWEVER, if you do it yourself, keep in mind that the parent of the bridge parts which have box colliders, must have a Rigidbody on it, with "Use Gravity" disabled and "Is Kinematic" enabled.

