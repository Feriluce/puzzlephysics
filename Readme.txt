In order to open up the first scene of the game, go to the Assets folder and open PF1.unity



How to Add, Commit and Sync with GitHub for Windows and its Powershell:


If you have uncommited changes in "GitHub for Windows", and you wrote the title and the description and you clicked on "Commit", and after a while it says "Commit Failed":
-Make sure everything regarding Unity is closed. (the editor, the code explorer, photoshop with open texture from project etc)
-Click on the OPEN SHELL button on the error message to open a powershell for the project directory.
-type and hit Enter: git status

Wait a sec, and you will see a list of things written in red. Those are the uncommited files.
If you want to commit all of them (which is what you do want 90% of the time), 

-type: git add *

Note: if you want to add just one specific file from the list, write: git add [file name]

Wait...

If you want to make sure all files have been added, 
-type again: git status 

If all the files you're interested in uploading are green, you're all good. otherwise go back to the previous steps.

-Now to commit, type: git commit

A notepad with a bunuch of text will open. 
-Write the Title of your commit on the first row, then under it the Description. (ignore the things surrounded with #'s in that file)
-Then Save the file (ctrl+s) and close it.
You can now close the powershell if you want.

-Now go back to GitHub for Windows, and all you have to do is sync it all up with the website. (press "sync" at the top of the window)

All done.

Note: if at any point changes in "GitHub for Windows" appear to not be refreshed, click the Back button and go back to the program's dashboard. Then doubleclick on puzzlephysics again.




